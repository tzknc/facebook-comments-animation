PFont myFont;
JSONArray lines;
String commentAuthor = "NA";
PImage img;
import gifAnimation.*;
JSONArray comments;
PImage[] animation;
Gif loopingGif;
JSONObject comment;
int i=0, j=0, ite = 0;
int x=10, y=55; // the line height
String message;
Boolean changeComment = true;
Boolean showEmoji = false;
int commentPosition = 0;
int timerToFade = 0;
int authorNameLength;
int anlc = 22;
String emoji = "like";
int alphaEmoji = 0;
boolean emojiChange = false;
int[] emojiAlphas = new int[0];
int currentEmo = 0;
boolean running = true;



void setup()
{
  size(1200, 600);  
  comments = loadJSONArray("comments.json");
  background(255); 
  frameRate(25);
  myFont = createFont("Avenir", 40, true);
  textFont(myFont);
  textAlign(LEFT, LEFT);
  smooth();
  loopingGif = new Gif(this, "fb.gif");
  loopingGif.loop();
}

void draw() {
  commentsSwitcher();
  ite++;
  update();
  //saveFrame("export3/line-######.png");
}

// this takes care of switching the comments from one to another
// by loading it from the json when needed.
void commentsSwitcher () {
  if (changeComment == true) {
    println("current position: " + commentPosition);
    println("out of: " + comments.size() );
    println(" - ");
    comment = comments.getJSONObject(commentPosition);
    commentAuthor = comment.getString("name");
    lines = comment.getJSONArray("lines"); 
    int fix = comment.getInt("minus");   
    authorNameLength = (commentAuthor.length() - fix);
    emojiChange = true; // resetting the emojis
    emojiAlphas = new int[0];
    currentEmo = 0;
    changeComment = false;
  }
}

void update () {
  
  if (ite >= 2) { // the speed of updating
    background(105,206,105); 
    drawPlaceholder();
    
    if(showEmoji) {  
      textSize(18);
      textAlign(CENTER, CENTER);

      if (emojiChange == true) { 
        prepareEmojiAlpha();
        emojiChange = false;
        //println(emojiAlphas);
      }
      drawEmoji();
    }
    textAlign(LEFT, LEFT);
    textSize(40);
    type();
    displayName();
    displayTyped();
    ite = 0;
  }
}
void drawPlaceholder() {
  fill(255);
  noStroke();
  int sizey = 80 + (65 * i);
  rect(30, 100, 1150, sizey , 7);
}
void displayName () {
  changeTypeColor(1);
  text(commentAuthor, x, 0);
}

void displayTyped () {
  //translate(0, 20);   
  changeTypeColor(0);
  if (i > 0) {
    for (int q = 0; q < i; q = q+1) {
      String previousLine = lines.getString(q);
      if (q == 0) { 
        text(previousLine, x + (authorNameLength * anlc), y*q); 
      } else {
        text(previousLine, x, y*q);
      }
      //text(previousLine, x, y*q);
    }
  }
}

void type () {
  translate(50, 150);
  changeTypeColor(0);
  //-----------------------------
  if (i<lines.size() ) {
    message = lines.getString(i); // getString returns string from given position.. weird AF!
    if (j < message.length()) {
      String s1 = message.substring(0, j);
      if (i == 0) { 
        text(s1, x + (authorNameLength * anlc), y*i); 
      } else {
        text(s1, x, y*i);
      }
      
      j++;
    } else { 
      j=0;
      i++;
    }
  } else if (i >= lines.size() ) {   // size() = length...
    // stop completely
    if ((commentPosition + 1) >= comments.size()) {
      stopNow();
    }
    if(timerToFade == 100) {
    showEmoji = false;
    changeTheCommentResetTheLoop();
    timerToFade = 0;
    } else {
      timerToFade++;
      showEmoji = true;
    }
  }
}


void changeTypeColor(int x) {
  if (x == 1) {
    fill(113, 160, 234); // facebook blue
  } else {
    fill(0, 0, 0);
  }
}

void stopNow() {
   println("stopped");
   noLoop();
   running = false;
   return;
}

void changeTheCommentResetTheLoop() {
   
   changeComment = true;
   commentPosition++;
   //reset the loop
   i=0;
   j=0;
   ite = 0;
   alphaEmoji = 0;
}

void prepareEmojiAlpha() { 
   int i = 0;
   
   int like = comment.getInt("like");   
   if ( like > 0) {
     emojiAlphas = append(emojiAlphas, 0);
     //emojiAlphas[i] = 0;
     i++;
   }
   
   int love = comment.getInt("love");   
   if ( love > 0) { 
     //emojiAlphas[i] = 0;
     emojiAlphas = append(emojiAlphas, 0);
     i++;
   }
   
   int haha = comment.getInt("haha");   
   if ( haha > 0) { 
     //emojiAlphas[i] = 0;
     emojiAlphas = append(emojiAlphas, 0);
     i++;
   }
   
   int yay = comment.getInt("yay");   
   if ( yay > 0) { 
     //emojiAlphas[i] = 0;
     emojiAlphas = append(emojiAlphas, 0);
     i++;
   }
   
   int wow = comment.getInt("wow");   
   if ( wow > 0) { 
     //emojiAlphas[i] = 0;
     emojiAlphas = append(emojiAlphas, 0);
     i++;
   }
   
   int sad = comment.getInt("sad");   
   if ( sad > 0) { 
     //emojiAlphas[i] = 0;
     emojiAlphas = append(emojiAlphas, 0);
     i++;
   }
   
   int angry = comment.getInt("angry");   
   if ( angry > 0) { 
     //emojiAlphas[i] = 0;
     emojiAlphas = append(emojiAlphas, 0);
     i++;
   }
}


int returnAlpha(int pos) { 
  // dostane alpha
  // dostane pozici v earray
  int calculatedAlpha = 0; 
  if (pos == currentEmo) {
    calculatedAlpha = emojiAlphas[pos] + 18;
    emojiAlphas[pos] = calculatedAlpha;
    if (calculatedAlpha > 250) {
      currentEmo ++;
    }
  } else {
    calculatedAlpha = emojiAlphas[pos];
  }
  
  return calculatedAlpha;
}
  
  
void drawEmoji() {
   int i = 0;
   if( alphaEmoji <= 250) {
       alphaEmoji = alphaEmoji + 15;
   }
      
   int like = comment.getInt("like");   
   if ( like > 0) {     
     int alpha = returnAlpha(i);
     placeEmoji(90, i, like, alpha);
     
     i++;
   }
   
   int love = comment.getInt("love");   
   if ( love > 0) { 
     int alpha = returnAlpha(i);
     placeEmoji(230, i, love, alpha);
     i++;
   }
   
   int haha = comment.getInt("haha");   
   if ( haha > 0) { 
     int alpha = returnAlpha(i);
     placeEmoji (370, i, haha, alpha);
     i++;
   }
   
   int yay = comment.getInt("yay");   
   if ( yay > 0) { 
     int alpha = returnAlpha(i);
     placeEmoji (510, i, yay, alpha);
     i++;
   }
   
   int wow = comment.getInt("wow");   
   if ( wow > 0) { 
     int alpha = returnAlpha(i);
     placeEmoji (650, i, wow, alpha);
     i++;
   }
   
   int sad = comment.getInt("sad");   
   if ( sad > 0) { 
     int alpha = returnAlpha(i);
     placeEmoji (790, i, sad, alpha);
     i++;
   }
   
   int angry = comment.getInt("angry");   
   if ( angry > 0) { 
     int alpha = returnAlpha(i);
     placeEmoji (930, i, angry, alpha);
     i++;
   }
}

void placeEmoji (int emoPos, int i, int amount, int emojiAlpha) {
     int a = 90;
     int b = 120;
     int c = 150;
     int xpos = 70;

     a = a + (140*i);
     xpos= 60 *i;
     // - - 
     PImage croppedEmoji = loopingGif.get(emoPos, b, c, c); 
     croppedEmoji.resize(0, 60);
     int Ypos = 92+(65 * lines.size());
     tint(255, emojiAlpha);
     image(croppedEmoji, 1100 - xpos, Ypos);
     // writing the amounts
     
     if (amount > 1) { 
       fill(99,100,103,emojiAlpha);
       //textSize(12);
       text(amount, 1129 - xpos, Ypos + 65); 
       //textSize(40);
     }
}

void mousePressed() {  
  //stop the noise generator and the filter
  if( running == true) {
    stopNow();
  } else {
    running = true;
    loop();
  }
  
}
